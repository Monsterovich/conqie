#ifndef COMMON_H
#define COMMON_H

#include <QWindow>

#include <QObject>
#include <QQuickItem>
#include <QString>
#include <QSettings>
#include <QQmlContext>
#include <QProcess>
#include <QFile>
#include <QByteArray>
#include <QMetaObject>
#include <QDebug>
#include <QCommandLineParser>
#include <QStorageInfo>
#include <QStandardPaths>

#include <QX11Info>

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>

#include <X11/extensions/shape.h>
#include <X11/extensions/Xfixes.h>

#endif // COMMON_H
