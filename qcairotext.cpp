#include "qcairotext.h"

#include <QImage>
#include <QQuickWindow>
#include <QSGSimpleTextureNode>
#include <QApplication>
#include <QRgb>

QCairoText::QCairoText(QQuickItem *parent)
{
	(void)parent;

	m_image = nullptr;
	m_font_family = "Arial";
	m_font_slant = CAIRO_FONT_SLANT_NORMAL;
	m_font_weight = CAIRO_FONT_WEIGHT_NORMAL;
	m_font_size = 16.0f;
	m_x_offset = 0.0f;
	m_y_offset = 0.0f;
	m_color.setRgbF(1.0f, 1.0f, 1.0f);
	m_horizontal_alignment = AlignHCenter;
	m_vertical_alignment = AlignVCenter;
	m_antialiasing = AntialiasDefault;

	setWidth(256);
	setHeight(256);

	setFlag(Flag::ItemHasContents);
}

QCairoText::~QCairoText() {
	delete m_image;

	cairo_destroy(m_cr);
	cairo_surface_destroy(m_surf);
}

void QCairoText::componentComplete()
{
	QQuickItem::componentComplete();

	if (isComponentComplete())
	{
		m_image = new QImage(width(), height(), QImage::Format_RGB32);

		m_surf = cairo_image_surface_create_for_data(m_image->bits(), CAIRO_FORMAT_RGB24,
																	width(), height(), m_image->bytesPerLine());
		m_cr = cairo_create(m_surf);
	}
}

void QCairoText::setText(const QString &text) {
	m_text = text;
	update();
}

QSGNode *QCairoText::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
	assert(m_image);
	assert(m_surf);
	assert(m_cr);

	// fill framebuffer with transparent pixels
	if (qEnvironmentVariableIntValue("CONQIE_DEBUG_LAYER")) {
		cairo_set_source_rgba(m_cr, 1.0f, 0.0f, 0.0f, 0.5f);
	} else {
		cairo_set_source_rgba(m_cr, 0.0f, 0.0f, 0.0f, 0.0f);
	}

	cairo_set_operator(m_cr, CAIRO_OPERATOR_SOURCE);
	cairo_rectangle(m_cr, 0, 0, width(), height());
	cairo_fill(m_cr);

	cairo_set_source_rgb(m_cr, m_color.redF(), m_color.greenF(), m_color.blueF());

	cairo_set_antialias(m_cr, (cairo_antialias_t)m_antialiasing);

	cairo_select_font_face(m_cr, m_font_family.toStdString().c_str(),
		(cairo_font_slant_t)m_font_slant,
		(cairo_font_weight_t)m_font_weight);

	cairo_set_font_size(m_cr, m_font_size);

	cairo_text_extents_t extents;
	cairo_text_extents(m_cr, m_text.toStdString().c_str(), &extents);

	double x = 0.0f, y = 0.0f;

	switch (m_horizontal_alignment) {
		case AlignHCenter:
			x = width() * 0.5 - extents.x_bearing - extents.width * 0.5;
			break;
		case AlignRight:
			x = width() - extents.x_bearing - extents.width;
		case AlignLeft:
		default:
			break;
	}

	switch (m_vertical_alignment) {
		case AlignTop:
			y = extents.height;
			break;
		case AlignVCenter:
			y = height() * 0.5 - extents.y_bearing - extents.height * 0.5;
			break;
		case AlignBottom:
			y = height() - extents.y_bearing - extents.height;
		default:
			break;
	}

	cairo_move_to(m_cr, x + m_x_offset,  y + m_y_offset);
	cairo_show_text(m_cr, m_text.toStdString().c_str());

	QSGSimpleTextureNode *node = (QSGSimpleTextureNode*)oldNode;

	if (!node) {
		node = new QSGSimpleTextureNode();
	}

	QSGTexture *texture = window()->createTextureFromImage(*m_image);
	delete node->texture();
	node->setTexture(texture);

	node->setRect(boundingRect());

	return node;
}

void QCairoText::registerQML() {
	qmlRegisterType<QCairoText>("QtCairoText", 1, 0, "CairoText");
}
