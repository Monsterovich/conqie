#ifndef MAIN_H
#define MAIN_H

#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "common.h"

class QmlCBridge : public QObject {
	Q_OBJECT
public:
	QmlCBridge(const QString &iniFile);
	~QmlCBridge();

	void setComponent(QObject *_component);
public slots:
	Q_INVOKABLE void test();
	Q_INVOKABLE QString executeCommand(const QString &command, const QStringList &arguments = QStringList());
	Q_INVOKABLE QVariant getSettingsValue(const QString &key, const QVariant &defaultValue, const QString &prefix);
	Q_INVOKABLE QVariantMap getStorageInfo(const QString &path);
	Q_INVOKABLE QString readFile(const QString &path);

private:
	QString getThemeCode();
	//void sendClientMessage(const QString &message, quint64 data0, quint64 data1 = 0, quint64 data2 = 0, quint64 data3 = 0, quint64 data4 = 0);
private:
	QObject *component;
	QSettings *settings;
};

#endif // MAIN_H
