# Conqie

![Logo](conqie.png)

Light-weight system monitor written in Qt 5 (5.12.8). Alternative to [Conky](https://github.com/brndnmtthws/conky).

### Motivation
- More efficiency (less CPU usage, hardware rendering)
- Fewer glitches
- Uses the full set of Qt and QML features (you can interact with widget + sexy effects!)

### Demo video

![Sample Video](demo/demo.mp4)

### How to build

1. Install Qt libraries (also do `sudo apt install qml-module-qtquick-window2 qml-module-qtquick2 qml-module-qtquick-layouts qml-module-qtgraphicaleffects qml-module-qtquick-controls qml-module-qtquick-controls2 libcairo2-dev`).
2. ```qmake```
3. ```make```

### How to run

1. Copy default theme from [repository](https://gitlab.com/Monsterovich/conqie/-/tree/master/defaultTheme) to `$HOME/.config/conqie`.
2. Run conqie.


### Releases

[Download](https://gitlab.com/Monsterovich/conqie/-/releases)
