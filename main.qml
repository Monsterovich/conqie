import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

import QtCairoText 1.0

Window {
    id: appWindow
    visible: true
    title: "Conqie"
    //flags: Qt.Tool | Qt.FramelessWindowHint | Qt.WindowStaysOnBottomHint | Qt.WA_TranslucentBackground
    flags: Qt.CustomizeWindowHint | Qt.WindowStaysOnBottomHint | Qt.Tool
    color: "transparent"

    function componentLoaded(themeCode) {
        let theme = Qt.createQmlObject(themeCode, appWindow, "theme");

        if (!theme) {
            Qt.quit();
            return;
        }

        x = app.getSettingsValue("x", 0, "Window");
        y = app.getSettingsValue("y", 0, "Window");
        width = app.getSettingsValue("width", 640, "Window");
        height = app.getSettingsValue("height", 480, "Window");

        console.log("x: ", x);
        console.log("y: ", y);
        console.log("width: ", width);
        console.log("height: ", height);
    }

    //
    // Shorthands
    //

//    function formatBytes(bytes, decimals = 2) {
//        const sizes = [qsTr("Bytes"), qsTr("KB"), qsTr("MB"), qsTr("GB"), qsTr("TB"), qsTr("PB"), qsTr("EB"), qsTr("ZB"), qsTr("YB")]

//        if (bytes === 0) {
//            return "0 " + sizes[0];
//        }

//        const k = 1024
//        const dm = decimals < 0 ? 0 : decimals
//        const i = Math.floor(Math.log(bytes) / Math.log(k))
//        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i]
//    }

    function formatBytes(bytes, decimals = 2, binaryUnits = true) {
        let unitMultiple = binaryUnits ? 1024 : 1000;
        let unitNames = unitMultiple === 1024 ? ['Bytes', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'] : ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        if(bytes === 0) {
            return "0 " + unitNames[0];
        }

        let unitChanges = Math.floor(Math.log(bytes) / Math.log(unitMultiple));
        return parseFloat((bytes / Math.pow(unitMultiple, unitChanges)).toFixed(decimals || 0)) + ' ' + unitNames[unitChanges];
    }


    function getCPULoads() {
        let stat = app.readFile("/proc/stat");

        let lines = stat.split("\n");
        let cpus = [];

        for (let i = 0; i < getCPUCount() + 1; i++) {
            cpus.push(_calculateCPULoad(lines[i]));
        }

        return cpus;
    }

    function getCPUCount() {
        return Number(app.executeCommand("nproc", ["--all"]));
    }

    property var _lastValues: ({})

    function _calculateCPULoad(line) {
        let values = line.split(" ");

        const cpuId = values[0]
        if (cpuId === "cpu") {
            values.splice(1, 1);
        }

        for (let i = 1; i < 10; i++) {
            values[i] = Number(values[i]);
        }

        if (!_lastValues[cpuId]) {
            _lastValues[cpuId] = [cpuId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        }

        const lastValues = _lastValues[cpuId];
        let result = ((values[1]+values[2]+values[3]) - (lastValues[1]+lastValues[2]+lastValues[3])) / ((values[1]+values[2]+values[3]+values[4]) - (lastValues[1]+lastValues[2]+lastValues[3]+lastValues[4]));

        _lastValues[cpuId] = values;
        return result;
    }

    function formatPercents(perc) {
        return Math.floor(perc * 100) + "%";
    }

    function getMemoryInfo() {
        let info = {};
        let meminfo = app.readFile("/proc/meminfo");

        meminfo.split(/\n/g).forEach(function(line){
            line = line.split(':');

            if (line.length < 2) {
                return;
            }

            info[line[0]] = parseInt(line[1].trim(), 10) * 1024;
        });

        return info;
    }

    function getCPUClockSpeed() {
        const clocks = [];

        for (let i = 0; i < getCPUCount(); i++) {
            clocks[i] = `${Math.round(Number(app.readFile(`/sys/devices/system/cpu/cpu${i}/cpufreq/scaling_cur_freq`)) * 0.001)} MHz`;
        }

        return clocks;
    }
}
