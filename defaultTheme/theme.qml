import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

import QtCairoText 1.0

import "theme:/" as Theme

Item {
    anchors.fill: parent
    layer.enabled: true
    layer.effect: DropShadow {
        radius: 5
        samples: 12
        color: "#000000"
        spread: 0.1
    }

    // for debugging
//    Rectangle {
//        anchors.fill: parent
//        color: "#22FF0000"
//        Component.onCompleted: {
//            hoverController.enabled = false
//        }
//    }

    function render() {
        let date = new Date();

        time.text = Qt.formatDateTime(date, "hh:mm");
        day.text = Qt.formatDateTime(date, "dd");
        monthYear.text = Qt.formatDateTime(date, "MMMM yyyy");
        let _dayName = Qt.formatDateTime(date, "dddd");
        _dayName = _dayName.replace(/^./, _dayName.charAt(0).toUpperCase());
        dayName.text = _dayName;

        let rootStorageInfo = app.getStorageInfo("/")
        rootDiskSpace.text = formatBytes(rootStorageInfo.bytesAvailable) + " / " + formatBytes(rootStorageInfo.bytesTotal);

        let memoryInfo = getMemoryInfo();
        memoryUsage.text = formatBytes(memoryInfo.MemTotal - memoryInfo.MemAvailable) + " / " + formatBytes(memoryInfo.MemTotal);

        let cpuLoads = getCPULoads();
        cpuUsage.text = formatPercents(cpuLoads[0]);

        let cpuClocks = getCPUClockSpeed();

        for (let i = 0; i < getCPUCount(); i++) {
            cpuRepeater.itemAt(i).load = cpuLoads[i+1];
            cpuRepeater.itemAt(i).clockSpeed = cpuClocks[i];
        }

        let ipsString = app.executeCommand("hostname", ["-I"]);
        ipsString = ipsString.slice(0, -2); // remove garbage symbol and space
        let ips = ipsString.split(" ");
        ipAddresses.model = ips

        timer.start()
    }

    property int renderInterval: 1000

    Timer {
        id: timer
        interval: renderInterval
        repeat: false
        running: true
        onTriggered: render()
    }

    Component.onCompleted: render()

    readonly property string color1: "#99CCFF"
    readonly property string color2: "#EAEAEA"
    readonly property string font1: "GE Inspira"
    readonly property string font2: "Ubuntu"
    readonly property real radiusBase: 1.2

    NumberAnimation on opacity {
        id: fadeOutEffect
        running: false
        to: 0.25
        duration: 250
    }

    NumberAnimation on opacity {
        id: fadeInEffect
        running: false
        to: 1.0
        duration: 500
    }

    Rectangle {
        id: hoverController
        anchors.fill: parent
        color: "transparent"
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                fadeOutEffect.start()
            }
            onExited: {
                fadeInEffect.start()
            }
        }
    }

    CairoText {
        id: time
        width: 256
        height: 128
        anchors.left: parent.left
        anchors.top: parent.top
        fontFamily: font1
        fontSize: 104
        layer.enabled: true
        layer.effect: Theme.CircularGlow {
            radius: radiusBase / 112
        }
    }

    CairoText {
        id: day
        width: 48
        height: 32
        anchors.left: time.right
        anchors.leftMargin: 25
        anchors.top: time.top
        anchors.topMargin: 26
        color: color1
        fontFamily: font1
        fontSize: 40
        layer.enabled: true
        layer.effect: Theme.CircularGlow {
            radius: radiusBase / 40
        }
    }

    CairoText {
        id: monthYear
        width: 128
        height: 32
        anchors.left: day.right
        anchors.leftMargin: -2
        anchors.top: day.top
        anchors.topMargin: 6
        color: color2
        fontFamily: font1
        fontSize: 20
        layer.enabled: true
        layer.effect: Theme.CircularGlow {
            radius: radiusBase / 60
        }
    }

    CairoText {
        id: dayName
        width: 256
        height: 48
        anchors.left: time.right
        anchors.leftMargin: 40
        anchors.top: time.top
        anchors.topMargin: 72
        color: color2
        fontFamily: font1
        fontSize: 38
        horizontalAlignment: CairoText.AlignLeft
        layer.enabled: true
        layer.effect: Theme.CircularGlow {
            radius: radiusBase / 96
        }
    }

    Grid {
        id: systemInfo
        anchors.top: time.bottom
        anchors.topMargin: 10
        anchors.left: time.left
        anchors.leftMargin: 15
        columns: 3
        spacing: 30
        RowLayout {
            CairoText {
                id: rootDiskSpaceTitle
                width: 72
                height: 24
                text: "Disk space"
                color: color1
                fontFamily: font1
                fontSize: 14
                fontWeight: CairoText.WeightBold
                layer.enabled: true
                layer.effect: Theme.CircularGlow {
                    radius: radiusBase / 32
                }
            }
            CairoText {
                id: rootDiskSpace
                Layout.leftMargin: 6
                width: 148
                height: 24
                color: color2
                fontFamily: font1
                fontSize: 14
                horizontalAlignment: CairoText.AlignLeft
                fontWeight: CairoText.WeightBold
                layer.enabled: true
                layer.effect: Theme.CircularGlow {
                    radius: radiusBase / 64
                }
            }
        }
        RowLayout {
            CairoText {
                id: memoryUsageTitle
                width: 32
                height: 24
                text: "RAM"
                color: color1
                fontFamily: font1
                fontSize: 14
                fontWeight: CairoText.WeightBold
                layer.enabled: true
                layer.effect: Theme.CircularGlow {
                    radius: radiusBase / 32
                }
            }
            CairoText {
                id: memoryUsage
                Layout.leftMargin: 6
                width: 128
                height: 24
                text: "RAM"
                color: color2
                fontFamily: font1
                fontSize: 14
                horizontalAlignment: CairoText.AlignLeft
                fontWeight: CairoText.WeightBold
                layer.enabled: true
                layer.effect: Theme.CircularGlow {
                    radius: radiusBase / 64
                }
            }
        }
        RowLayout {
            CairoText {
                id: cpuUsageTitle
                width: 32
                height: 24
                text: "CPU"
                color: color1
                fontFamily: font1
                fontSize: 14
                fontWeight: CairoText.WeightBold
                layer.enabled: true
                layer.effect: Theme.CircularGlow {
                    radius: radiusBase / 32
                }
            }
            CairoText {
                id: cpuUsage
                Layout.leftMargin: 6
                width: 42
                height: 24
                color: color2
                fontFamily: font1
                fontSize: 14
                horizontalAlignment: CairoText.AlignLeft
                fontWeight: CairoText.WeightBold
                layer.enabled: true
                layer.effect: Theme.CircularGlow {
                    radius: radiusBase / 32
                }
            }
        }
    }

    Column {
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: 32
        anchors.rightMargin: 64
        RowLayout {
            CairoText {
                Layout.preferredWidth: 86
                Layout.alignment: Qt.AlignLeft
                text: "PROCESSORS"
                width: 86
                height: 24
                color: color2
                fontFamily: font1
                fontSize: 14
                horizontalAlignment: CairoText.AlignLeft
                fontWeight: CairoText.WeightBold
                layer.enabled: true
                layer.effect: Theme.CircularGlow {
                    radius: radiusBase / 48
                }
            }
            Rectangle {
                Layout.alignment: Qt.AlignBottom
                Layout.bottomMargin: 4
                color: color2
                width: 320
                height: 2
                radius: 2
                layer.enabled: true
                layer.effect: Theme.CircularGlow {}
            }
        }
        Grid {
            columns: 2
            rows: getCPUCount() / columns
            rowSpacing: 2
            columnSpacing: 5
            Repeater {
                id: cpuRepeater
                model: getCPUCount()
                RowLayout {
                    width: 196
                    property real load: 0.0
                    property string clockSpeed: ""
                    CairoText {
                        Layout.preferredWidth: 48
                        Layout.alignment: Qt.AlignLeft
                        width: 48
                        height: 24
                        text: `CPU${index + 1}:`
                        color: color1
                        fontFamily: font1
                        fontSize: 14
                        fontWeight: CairoText.WeightBold
                        horizontalAlignment: CairoText.AlignLeft
                        layer.enabled: true
                        layer.effect: Theme.CircularGlow {
                            radius: radiusBase / 32
                        }
                    }
                    CairoText {
                        Layout.alignment: Qt.AlignCenter
                        Layout.rightMargin: 12
                        text: parent.clockSpeed
                        width: 64
                        height: 24
                        color: color2
                        fontFamily: font1
                        fontSize: 14
                        horizontalAlignment: CairoText.AlignLeft
                        fontWeight: CairoText.WeightBold
                        layer.enabled: true
                        layer.effect: Theme.CircularGlow {
                            radius: radiusBase / 32
                        }
                    }
                    Theme.CircularProgressBar {
                        function _getColor(p) {
                            if (p <= 0.25) {
                                return "#00B800";
                            } else if (p <= 0.5) {
                                return "#FFFF00";
                            } else if (p <= 0.75) {
                                return "#FF9000";
                            } else if (p <= 1.0) {
                                return "#FF2600";
                            }
                        }
                        Layout.alignment: Qt.AlignRight
                        value: parent.load
                        progressColor: _getColor(parent.load)
                        animationDuration: 0
                        size: 25
                        layer.enabled: true
                        layer.effect: Theme.CircularGlow {}
                    }
                    CairoText {
                        Layout.preferredWidth: 36
                        Layout.alignment: Qt.AlignLeft
                        text: `${Math.round(parent.load * 100)}%`
                        width: 36
                        height: 24
                        color: color2
                        fontFamily: font1
                        fontSize: 14
                        fontWeight: CairoText.WeightBold
                        layer.enabled: true
                        layer.effect: Theme.CircularGlow {
                            radius: radiusBase / 32
                        }
                    }
                }
            }
        }
        RowLayout {
            CairoText {
                Layout.preferredWidth: 66
                Layout.alignment: Qt.AlignLeft
                text: "NETWORK"
                width: 66
                height: 24
                color: color2
                fontFamily: font1
                fontSize: 14
                horizontalAlignment: CairoText.AlignLeft
                fontWeight: CairoText.WeightBold
                layer.enabled: true
                layer.effect: Theme.CircularGlow {
                    radius: radiusBase / 48
                }
            }
            Rectangle {
                Layout.alignment: Qt.AlignBottom
                Layout.bottomMargin: 4
                color: color2
                width: 340
                height: 2
                radius: 2
                layer.enabled: true
                layer.effect: Theme.CircularGlow {}
            }
        }
        Column {
            RowLayout {
                CairoText {
                    Layout.preferredWidth: 64
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    width: 64
                    height: 24
                    text: "IP:"
                    color: color1
                    fontFamily: font1
                    fontSize: 14
                    fontWeight: CairoText.WeightBold
                    horizontalAlignment: CairoText.AlignLeft
                    layer.enabled: true
                    layer.effect: Theme.CircularGlow {
                        radius: radiusBase / 32
                    }
                }
                ColumnLayout {
                    spacing: 0
                    Repeater {
                        id: ipAddresses
                        CairoText {
                            Layout.preferredWidth: 128
                            Layout.alignment: Qt.AlignRight
                            Layout.leftMargin: 220
                            width: 128
                            height: 24
                            text: modelData
                            color: color2
                            fontFamily: font1
                            fontSize: 14
                            fontWeight: CairoText.WeightBold
                            horizontalAlignment: CairoText.AlignLeft
                            layer.enabled: true
                            layer.effect: Theme.CircularGlow {
                                radius: radiusBase / 64
                            }
                        }
                    }
                }
            }
        }
    }
}
