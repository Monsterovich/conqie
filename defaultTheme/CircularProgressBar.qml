import QtQuick 2.0

Item {
    id: rootItem

    property int size: 128

    width: size
    height: width

    property int lineWidth: 5
    property real value

    property color progressColor: "black"
    property color circleColor: "lightgray"
    property string lineCap: "round"

    property int animationDuration: 1000

    onValueChanged: {
        canvas.degree = value * 360;
    }

    Canvas {
        id: canvas
        anchors.fill: parent
        width: 256
        height: 256

        property real degree: 0

        Behavior on degree {
            NumberAnimation {
                duration: rootItem.animationDuration
            }
        }

        antialiasing: true
        renderStrategy: Canvas.Cooperative

        onDegreeChanged: {
            requestPaint();
        }

        onPaint: {
            const ctx = getContext("2d");
            ctx.reset();

            const radius = rootItem.size * 0.5 - rootItem.lineWidth
            const startAngle = Math.PI / 180 * 270;
            const endAngle = Math.PI / 180 * (270 + 360);
            const progressAngle = Math.PI / 180 * (270 + degree);

            const x = rootItem.width * 0.5;
            const y = rootItem.height * 0.5;

            ctx.lineCap = rootItem.lineCap;
            ctx.lineWidth = rootItem.lineWidth;

            ctx.beginPath();
            ctx.arc(x, y, radius, startAngle, endAngle);
            ctx.strokeStyle = rootItem.circleColor;
            ctx.stroke();

            ctx.beginPath();
            ctx.arc(x, y, radius, startAngle, progressAngle);
            ctx.strokeStyle = rootItem.progressColor;
            ctx.stroke();
        }
    }
}
