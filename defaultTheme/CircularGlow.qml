import QtQuick 2.0

ShaderEffect {
    property variant source
    property int samples: 8
    property real radius: 0.01
    property real alpha: 0.04
    property bool highQuality: true

    vertexShader: "
        uniform highp mat4 qt_Matrix;
        attribute highp vec4 qt_Vertex;
        attribute highp vec2 qt_MultiTexCoord0;
        varying highp vec2 coord;
        void main() {
            coord = qt_MultiTexCoord0;
            gl_Position = qt_Matrix * qt_Vertex;
        }"
    fragmentShader: "
        varying highp vec2 coord;
        uniform sampler2D source;
        uniform lowp float qt_Opacity;

        uniform highp int samples;
        uniform highp float radius;
        uniform highp float alpha;
        uniform highp bool highQuality;

        const float pi_1 = 0.70710678118; // sin(pi/4),cos(pi/4)
        const float pi_2_x = 0.92387953251; // cos(pi/8)
        const float pi_2_y = 0.38268343236; // sin(pi/8)
        const float pi_3_x = pi_2_y;
        const float pi_3_y = pi_2_x;

        vec4 blendOver(vec4 t0, vec4 t1) {
            return t0 + t1 * (1.0 - t0.a);
        }

        void main() {
            vec4 output;

            const float amult2 = 0.5;

            float current_alpha = alpha * qt_Opacity;
            float current_radius = radius;

            if (highQuality) {
                current_radius *= 0.5;
            }

            for (int i = 0; i < samples; i++) {
                current_alpha *= 0.75;

                output = blendOver(output, texture2D(source, vec2(coord.x + current_radius, coord.y)) * current_alpha);
                output = blendOver(output, texture2D(source, vec2(coord.x, coord.y - current_radius)) * current_alpha);
                output = blendOver(output, texture2D(source, vec2(coord.x - current_radius, coord.y)) * current_alpha);
                output = blendOver(output, texture2D(source, vec2(coord.x, coord.y + current_radius)) * current_alpha);

                output = blendOver(output, texture2D(source, vec2(coord.x + current_radius * pi_1, coord.y + current_radius * pi_1)) * current_alpha);
                output = blendOver(output, texture2D(source, vec2(coord.x + current_radius * pi_1, coord.y - current_radius * pi_1)) * current_alpha);
                output = blendOver(output, texture2D(source, vec2(coord.x - current_radius * pi_1, coord.y - current_radius * pi_1)) * current_alpha);
                output = blendOver(output, texture2D(source, vec2(coord.x - current_radius * pi_1, coord.y + current_radius * pi_1)) * current_alpha);

                if (highQuality) {
                    output = blendOver(output, texture2D(source, vec2(coord.x + current_radius * pi_2_x, coord.y + current_radius * pi_2_y)) * current_alpha * amult2);
                    output = blendOver(output, texture2D(source, vec2(coord.x + current_radius * pi_2_x, coord.y - current_radius * pi_2_y)) * current_alpha * amult2);
                    output = blendOver(output, texture2D(source, vec2(coord.x - current_radius * pi_2_x, coord.y - current_radius * pi_2_y)) * current_alpha * amult2);
                    output = blendOver(output, texture2D(source, vec2(coord.x - current_radius * pi_2_x, coord.y + current_radius * pi_2_y)) * current_alpha * amult2);

                    output = blendOver(output, texture2D(source, vec2(coord.x + current_radius * pi_3_x, coord.y + current_radius * pi_3_y)) * current_alpha * amult2);
                    output = blendOver(output, texture2D(source, vec2(coord.x + current_radius * pi_3_x, coord.y - current_radius * pi_3_y)) * current_alpha * amult2);
                    output = blendOver(output, texture2D(source, vec2(coord.x - current_radius * pi_3_x, coord.y - current_radius * pi_3_y)) * current_alpha * amult2);
                    output = blendOver(output, texture2D(source, vec2(coord.x - current_radius * pi_3_x, coord.y + current_radius * pi_3_y)) * current_alpha * amult2);
                }

                current_radius *= 1.25;
            }

            output = blendOver(output, texture2D(source, coord) * qt_Opacity);

            gl_FragColor = output;
        }"
}
