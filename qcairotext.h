#ifndef QCAIROTEXT_H
#define QCAIROTEXT_H

#include <cairo/cairo.h>

#include <QObject>
#include <QQuickItem>
#include <QSGGeometryNode>
#include <QColor>

class QCairoText : public QQuickItem
{
	Q_OBJECT
	Q_PROPERTY(QString text WRITE setText MEMBER m_text)
	Q_PROPERTY(QString fontFamily MEMBER m_font_family)
	Q_PROPERTY(int fontSlant MEMBER m_font_slant)
	Q_PROPERTY(int fontWeight MEMBER m_font_weight)
	Q_PROPERTY(double fontSize MEMBER m_font_size)
	Q_PROPERTY(double xOffset MEMBER m_x_offset)
	Q_PROPERTY(double yOffset MEMBER m_y_offset)
	Q_PROPERTY(QColor color MEMBER m_color)
	Q_PROPERTY(HorizontalAlignment horizontalAlignment MEMBER m_horizontal_alignment)
	Q_PROPERTY(VerticalAlignment verticalAlignment MEMBER m_vertical_alignment)
	Q_PROPERTY(Antialiasing antialiasing MEMBER m_antialiasing)
public:
	explicit QCairoText(QQuickItem *parent = 0);
	~QCairoText();
	void componentComplete();
	void setText(const QString &text);

	enum Weight {
		WeightNormal,
		WeightBold
	};
	Q_ENUM(Weight)

	enum Slant {
		SlantNormal,
		SlantItalic,
		SlantOblique
	};
	Q_ENUM(Slant)

	enum HorizontalAlignment {
		AlignLeft,
		AlignHCenter,
		AlignRight
	};
	Q_ENUM(HorizontalAlignment);

	enum VerticalAlignment {
		AlignTop,
		AlignVCenter,
		AlignBottom
	};
	Q_ENUM(VerticalAlignment);

	enum Antialiasing {
		AntialiasDefault,
		AntialiasNone,
		AntialiasGray,
		AntialiasSubpixel
	};
	Q_ENUM(Antialiasing);

	static void registerQML();
protected:
	virtual QSGNode *updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *);
private:
	QImage *m_image;

	cairo_surface_t *m_surf;
	cairo_t *m_cr;

	QString m_text, m_font_family;
	int m_font_slant, m_font_weight;
	double m_font_size, m_x_offset, m_y_offset;
	QColor m_color;
	HorizontalAlignment m_horizontal_alignment;
	VerticalAlignment m_vertical_alignment;
	Antialiasing m_antialiasing;
};

#endif // QCAIROTEXT_H
