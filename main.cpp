
#include "main.h"
#include "version.h"
#include "qcairotext.h"

QmlCBridge::QmlCBridge(const QString &iniFile) {
	QString *selectedPath = nullptr;
	QString iniFileInConfigPath = QStandardPaths::locate(QStandardPaths::ConfigLocation, QString("conqie") + QDir::separator() + iniFile);
	QStringList paths = QStringList() << iniFile << iniFileInConfigPath;
	for (auto &path : paths) {
		if (QFile::exists(path)) {
			selectedPath = &path;
			break;
		}
	}

	if (selectedPath) {
		settings = new QSettings(*selectedPath, QSettings::IniFormat);
	} else {
		qDebug() << iniFile << " is not found! Using defaults.";
		settings = new QSettings(iniFileInConfigPath, QSettings::IniFormat);
	}
}

void QmlCBridge::setComponent(QObject *_component)
{
	component = _component;

	WId window = qobject_cast<QWindow*>(component)->winId();
	Display *display = QX11Info::display();

	const QString WIdString = "0x" + QString::number(window, 16);
	qDebug() << "window id: " << WIdString;
	const QStringList arguments = {"-i", "-r", WIdString, "-b", "add,sticky"};
	QProcess::execute("wmctrl", arguments);

	//sendClientMessage("_NET_CURRENT_DESKTOP", 0xFFFFFFFF);

	// make window pass-through
	settings->beginGroup("Window");
	if (settings->value("clickthrough", false).toBool()) {
		XRectangle rect;
		XserverRegion region = XFixesCreateRegion(display, &rect, 1);
		XFixesSetWindowShapeRegion(display, window, ShapeInput, 0, 0, region);
		XFixesSetWindowShapeRegion (display, window, ShapeBounding, 0, 0, 0);
		XFixesDestroyRegion(display, region);
	}
	settings->endGroup();

	QMetaObject::invokeMethod(component, "componentLoaded", Qt::AutoConnection, Q_ARG(QVariant, getThemeCode()));
}

//void QmlCBridge::sendClientMessage(const QString &message, quint64 data0, quint64 data1, quint64 data2, quint64 data3, quint64 data4)
//{
//	Display *display = QX11Info::display();
//	WId window = qobject_cast<QWindow*>(component)->winId();

//	XEvent event;
//	long mask = SubstructureRedirectMask | SubstructureNotifyMask;

//	event.xclient.type = ClientMessage;
//	event.xclient.serial = 0;
//	event.xclient.send_event = True;
//	event.xclient.message_type = XInternAtom(display, message.toStdString().c_str(), False);
//	event.xclient.window = window;
//	event.xclient.format = 32;
//	event.xclient.data.l[0] = data0;
//	event.xclient.data.l[1] = data1;
//	event.xclient.data.l[2] = data2;
//	event.xclient.data.l[3] = data3;
//	event.xclient.data.l[4] = data4;

//	if (!XSendEvent(display, DefaultRootWindow(display), False, mask, &event)) {
//		qDebug() << "Cannot send message to window " << message;
//	}
//}

QVariantMap QmlCBridge::getStorageInfo(const QString &path) {
	QStorageInfo info((QDir(path)));

	return {
		{"bytesAvailable", info.bytesAvailable()},
		{"bytesFree", info.bytesFree()},
		{"bytesTotal", info.bytesTotal()},
		{"displayName", info.displayName()}
	};
}

QString QmlCBridge::readFile(const QString &path) {
	QFile file(path);
	if (!file.open(QFile::ReadOnly)) {
		qDebug() << "Couldn't open file " << path;
		return QString();
	}

	return QString::fromUtf8(file.readAll());
}


QString QmlCBridge::executeCommand(const QString &command, const QStringList &arguments) {
	QProcess process;
	process.start(command, arguments);
	process.waitForFinished(-1);

	return process.readAllStandardOutput();
}

QVariant QmlCBridge::getSettingsValue(const QString &key, const QVariant &defaultValue, const QString &prefix) {
	settings->beginGroup(prefix);
	QVariant value = settings->value(key, defaultValue);
	settings->endGroup();

	return value;
}

void QmlCBridge::test() {

}

QString QmlCBridge::getThemeCode() {
	settings->beginGroup("App");
	QString themeFilename = settings->value("theme", "theme.qml").toString();
	settings->endGroup();

	qDebug() << "theme:" << themeFilename;

	QString *selectedPath = nullptr;
	QString themeInConfigPath = QStandardPaths::locate(QStandardPaths::ConfigLocation, QString("conqie") + QDir::separator() + themeFilename);
	QStringList paths = QStringList() << themeFilename << themeInConfigPath;

	for (auto &path : paths) {
		if (QFile::exists(path)) {
			selectedPath = &path;
			break;
		}
	}

	QFile file;
	if (selectedPath) {
		file.setFileName(*selectedPath);
	} else {
		qDebug() << "Couldn't find theme file";
		return QString();
	}

	if (!file.open(QFile::ReadOnly)) {
		qDebug() << "Couldn't open theme file " << file.fileName();
		return QString();
	}

	return QString::fromUtf8(file.readAll());
}

QmlCBridge::~QmlCBridge() {
	settings->sync();
	delete settings;
}


int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

	qDebug() << "Running conqie version: " << "v" + QString(APP_VERSION) + "-" + QString(GIT_VERSION);

	if (!qEnvironmentVariableIntValue("CONQIE_VSYNC")) {
		QSurfaceFormat format = QSurfaceFormat::defaultFormat();
		format.setSwapInterval(0);
		QSurfaceFormat::setDefaultFormat(format);
	}

	QGuiApplication app(argc, argv);

	QCairoText::registerQML();
	QmlCBridge c_app("conqie.ini");

	QQmlApplicationEngine engine;

	const QUrl url(QStringLiteral("qrc:/main.qml"));
	QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
					 &app, [url](QObject *obj, const QUrl &objUrl) {
		if (!obj && url == objUrl)
			QCoreApplication::exit(-1);
	}, Qt::QueuedConnection);

	const QString importPathDir = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).at(0) + QDir::separator() + "conqie";
	QDir::setSearchPaths("theme", {
		":/theme", importPathDir
	});

	engine.load(url);

	QQmlContext *root = engine.rootContext();
	root->setContextProperty("app", &c_app);

	QObject *component = engine.rootObjects().at(0);
	c_app.setComponent(component);

	int status = app.exec();
	return status;
}
